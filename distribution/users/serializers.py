from rest_framework import serializers
from users.models import User


class UserSerializer(serializers.ModelSerializer):
    """Сериалайзер юзера"""
    class Meta:
        model = User
        fields = ('phone_number', 'operator_code', 'tag', 'timezone')
