from rest_framework.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def number_validator(value):
    """Валидатор номера телефона"""
    if value[0] != '7':
        raise ValidationError(
            _('Номер должен начинаться с 7'),
        )
    if len(value) < 11:
        raise ValidationError(
            _('Номер не полный'),
        )