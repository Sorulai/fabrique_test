import json
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from users.models import User


class UserTests(APITestCase):
    def setUp(self) -> None:
        self.valid_data = {
            'phone_number': '79601977278',
            'operator_code': '960',
            'tag': None,
            'timezone': 'Europe/Moscow'
        }
        self.invalid_number = {
            'phone_number': '89601977278',
            'operator_code': '960',
            'tag': None,
            'timezone': 'Europe/Moscow'
        }
        self.invalid_len_number = {
            'phone_number': '89601977278',
            'operator_code': '960',
            'tag': None,
            'timezone': 'Europe/Moscow'
        }
        self.user = User.objects.create(**self.valid_data)
        return super(UserTests, self).setUp()

    def test_add_user_correct(self):
        res = self.client.post(path=reverse('users:create'), data=json.dumps(self.valid_data),
                               content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_add_user_invalid_number(self):
        res = self.client.post(path=reverse('users:create'), data=json.dumps(self.invalid_number),
                               content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_add_user_invalid_len_number(self):
        res = self.client.post(path=reverse('users:create'), data=json.dumps(self.invalid_len_number),
                               content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_update_user_valid_data(self):
        update_data = {
            'phone_number': '79701977278',
        }
        res = self.client.put(path=reverse('users:update', kwargs={'pk': self.user.pk}), data=json.dumps(update_data),
                              content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_update_user_invalid_data(self):
        update_data = {
            'phone_number': '59701977278',
        }
        res = self.client.put(path=reverse('users:update', kwargs={'pk': self.user.pk}), data=json.dumps(update_data),
                              content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)

    def test_delete_user(self):
        res = self.client.delete(path=reverse('users:delete', kwargs={'pk': self.user.pk}),
                                 content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)

    def tearDown(self) -> None:
        return super(UserTests, self).tearDown()
