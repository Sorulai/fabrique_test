from django.urls import path
from users.views import UsersView

app_name = 'users'

urlpatterns = [
    path('create/', UsersView.as_view({'post': 'create'}), name='create'),
    path('update/<int:pk>/', UsersView.as_view({'put': 'update'}), name='update'),
    path('delete/<int:pk>/', UsersView.as_view({'delete': 'destroy'}), name='delete')

]
