import pytz
from django.db import models

from users.services import number_validator

NULLABLE = {'blank': True, 'null': True}


class User(models.Model):
    """Модель пользователя"""
    TIMEZONES = tuple(zip(pytz.all_timezones, pytz.all_timezones))

    phone_number = models.CharField(verbose_name='Номер телефона', unique=True, **NULLABLE, max_length=11,
                                    validators=[number_validator])
    operator_code = models.CharField(verbose_name='Код мобильного оператора', **NULLABLE, max_length=3)
    tag = models.CharField(verbose_name='Тег', **NULLABLE, max_length=30)
    timezone = models.CharField(verbose_name='Часовой пояс', max_length=32, choices=TIMEZONES, default='Europe/Moscow',
                                **NULLABLE)

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return f'{self.pk}'

    def save(self, *args, **kwargs):
        if self.operator_code is None or self.operator_code == 'str':
            self.operator_code = self.phone_number[1:4]
        if self.tag == 'string':
            self.tag = None
        super(User, self).save(*args, **kwargs)
