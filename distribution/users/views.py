from rest_framework import viewsets
from users.models import User
from users.serializers import UserSerializer


class UsersView(viewsets.ModelViewSet):
    """Представление пользователя(создание, обновление, удаление)"""
    queryset = User.objects.all().select_related()
    serializer_class = UserSerializer

