from django.conf import settings

from distribution.celery import app
from main.models import Mailing, Message
from main.services import send_mailing
from django.core.mail import send_mail


@app.task
def celery_send_msg(pk, mailing_filter, body):
    send_mailing(pk, mailing_filter, body)


@app.task
def celery_send_statistics():
    total_mailings = Mailing.objects.all().select_related().count()
    success_msg_count = Message.objects.filter(status=True).select_related().count()
    fail_msg_count = Message.objects.filter(status=False).select_related().count()

    str = f'За сутки было отправлено {total_mailings} рассылок. Успешно дошли {success_msg_count} сообщений, не дошли {fail_msg_count}'
    send_mail(
        subject='Statistics',
        message=str,
        from_email=settings.EMAIL_HOST_USER,
        recipient_list=[settings.EMAIL_ADMIN]
    )
