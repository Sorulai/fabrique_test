import datetime
import json

import pytz
from django.conf import settings
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from main.models import Mailing, Message
from users.models import User


class MailingTests(APITestCase):
    def setUp(self) -> None:
        self.tz_ny = pytz.timezone(settings.TIME_ZONE)
        current_date = self.tz_ny.localize(datetime.datetime.now())
        self.valid_data = {
            'date_mailing': current_date - datetime.timedelta(days=1),
            'body': 'hello',
            'mailing_filter': '960',
            'deadline': current_date + datetime.timedelta(days=2)
        }
        self.mailing = Mailing.objects.create(**self.valid_data)
        super(MailingTests, self).setUp()

    def test_add_mailing_valid_data(self):
        res = self.client.post(path=reverse('main:create'), data=json.dumps(self.valid_data, default=str),
                               content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_add_mailing_invalid_data(self):
        res = self.client.post(path=reverse('main:create'), data=json.dumps(self.valid_data, default=str),
                               content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

    def test_update_mailing(self):
        data = {
            'body': 'Hi',
            'mailing_filter': '990',
            'deadline': self.tz_ny.localize(datetime.datetime.now()) + datetime.timedelta(days=8)
        }
        res = self.client.put(path=reverse('main:update', kwargs={'pk': self.mailing}),
                              data=json.dumps(data, default=str),
                              content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_200_OK)

    def test_delete_mailing(self):
        res = self.client.delete(path=reverse('main:delete', kwargs={'pk': self.mailing}),
                              content_type='application/json')
        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)

    def tearDown(self) -> None:
        return super(MailingTests, self).tearDown()
