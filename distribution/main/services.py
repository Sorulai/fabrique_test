from datetime import datetime
import requests
from django.conf import settings
import main
from main.models import Mailing
from users.models import User


def send_mailing(pk, mailing_filter, body):
    """Отправка сообщений на сервис и запись сообщений"""
    users = User.objects.filter(operator_code=mailing_filter)
    mailing = Mailing.objects.filter(pk=pk).first()
    if not users:
        users = User.objects.filter(tag=mailing_filter)
    headers = {
        'Authorization': f'Bearer {settings.TOKEN}'

    }
    if users:
        for i in users:
            msg = main.models.Message.objects.filter(mailing=mailing, user=i, status=True)
            if not msg:
                msg = main.models.Message.objects.create(date=datetime.now(), mailing=mailing, user=i)
                try:
                    data = {
                        "id": msg.pk,
                        "phone": i.phone_number,
                        "text": body
                    }
                    response = requests.post(url=f'https://probe.fbrq.cloud/v1/send/{msg.pk}', headers=headers,
                                             json=data)
                    if response.status_code == 200:
                        msg.status = True
                        msg.save()
                except requests.exceptions.Timeout as e:
                    print(e)
                    pass
                except requests.exceptions.TooManyRedirects as e:
                    print(e)
                    pass
                except requests.ConnectionError as e:
                    print(e)
                    pass
            else:
                pass
