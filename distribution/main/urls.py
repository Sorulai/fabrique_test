from django.urls import path

from main.views import MailingView, StatisticsView, MessageDetail

app_name = 'main'

urlpatterns = [
    path('create/', MailingView.as_view({'post': 'create'}),name='create'),
    path('update/<int:pk>/', MailingView.as_view({'put': 'update'}),name='update'),
    path('delete/<int:pk>/', MailingView.as_view({'delete': 'destroy'}),name='delete'),

    path('statistics/', StatisticsView.as_view()),
    path('messages/<int:pk>/', MessageDetail.as_view())

]
