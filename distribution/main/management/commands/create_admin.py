from django.contrib.auth import get_user_model
from django.core.management import BaseCommand


class Command(BaseCommand):

    def handle(self, *args, **options):
        User = get_user_model()
        User.objects.create_superuser('admin', 'admin@gmail.com', 'admin')
