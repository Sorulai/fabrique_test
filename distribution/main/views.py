from rest_framework import viewsets
from rest_framework.generics import ListAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from main.models import Mailing, Message
from main.serializers import MailingSerializer, MessageDetailSerializer, StatisticsSerializer


class MailingView(viewsets.ModelViewSet):
    """Представление для CRUD рассылок"""
    queryset = Mailing.objects.all().select_related()
    serializer_class = MailingSerializer


class MessageDetail(ListAPIView):
    """Представление для получения деталей сообщений для одной рассылки"""
    serializer_class = MessageDetailSerializer

    def get_queryset(self):
        return Message.objects.filter(mailing_id=self.kwargs.get('pk'))


class StatisticsView(APIView):
    """Представление для получения статистики"""

    def get(self, request, *args, **kwargs):
        msgs = Message.objects.all().select_related()
        serializer = StatisticsSerializer(msgs.first())
        return Response(serializer.data)
