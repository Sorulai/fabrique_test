from rest_framework import serializers
from main.models import Mailing, Message
from users.serializers import UserSerializer


class MailingSerializer(serializers.ModelSerializer):
    """Сериалайзер рассылки"""

    class Meta:
        model = Mailing
        fields = ('date_mailing', 'body', 'mailing_filter', 'deadline')


class MessageDetailSerializer(serializers.ModelSerializer):
    """Сериалайзер сообщения по айди рассылки"""
    mailing = MailingSerializer()
    user = UserSerializer()

    class Meta:
        model = Message
        fields = ('pk', 'date', 'status', 'mailing', 'user')


class StatisticsSerializer(serializers.ModelSerializer):
    """Сериалайзер статистики"""
    total_mailings = serializers.SerializerMethodField()
    success = serializers.SerializerMethodField()
    fail = serializers.SerializerMethodField()

    class Meta:
        model = Message
        fields = ('total_mailings', 'success', 'fail')

    def get_total_mailings(self, instance):
        mailings_list = Mailing.objects.all().select_related()
        return mailings_list.count()

    def get_success(self, instance):
        success_list = Message.objects.filter(status=True).select_related()
        data = {
            'msg_count': success_list.count()
        }
        return data

    def get_fail(self, instance):
        fail_list = Message.objects.filter(status=False).select_related()
        data = {
            'msg_count': fail_list.count()
        }
        return data
