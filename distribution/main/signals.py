from datetime import datetime
import pytz
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver
from main.tasks import celery_send_msg
from main.models import Mailing


@receiver(post_save, sender=Mailing)
def signal_send_msg(sender, instance, **kwargs):
    """Сигнал после сохранения модели рассылки, для вызова функции отправки сообщений"""
    tz_ny = pytz.timezone(settings.TIME_ZONE)
    current_date = tz_ny.localize(datetime.now())
    if instance.date_mailing < current_date < instance.deadline:
        celery_send_msg.apply_async(
            kwargs={'pk': instance.pk, 'mailing_filter': instance.mailing_filter, 'body': instance.body},
            eta=instance.date_mailing, expires=instance.deadline, retry=True)
    elif instance.date_mailing > current_date:
        celery_send_msg.apply_async(
            kwargs={'pk': instance.pk, 'mailing_filter': instance.mailing_filter, 'body': instance.body},
            eta=instance.date_mailing, expires=instance.deadline, retry=True)
