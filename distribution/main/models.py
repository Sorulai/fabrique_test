from django.db import models
from users.models import User

NULLABLE = {'blank': True, 'null': True}


class Mailing(models.Model):
    """Модель рассылки"""
    date_mailing = models.DateTimeField(verbose_name='Дата и время запуска рассылки', **NULLABLE)
    body = models.TextField(verbose_name='Текст рассылки', **NULLABLE)
    mailing_filter = models.CharField(verbose_name='Фильтр рассылки', max_length=30, **NULLABLE)
    deadline = models.DateTimeField(verbose_name='Дата и время окончания рассылки', **NULLABLE)

    class Meta:
        verbose_name = 'Рассылка'
        verbose_name_plural = 'Рассылки'

    def __str__(self):
        return f'{self.pk}'


class Message(models.Model):
    """Модель сообщений"""
    date = models.DateTimeField(verbose_name='Дата и время отправки')
    status = models.BooleanField(verbose_name='Статус', default=False)
    mailing = models.ForeignKey(Mailing, on_delete=models.CASCADE, verbose_name='Рассылка', related_name='mailing')
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name='Пользователь', related_name='user')

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'
