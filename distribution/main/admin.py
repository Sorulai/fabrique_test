from django.contrib import admin

from main.models import Mailing, Message


@admin.register(Mailing)
class MailingAdmin(admin.ModelAdmin):
    list_display = ('pk', 'date_mailing', 'mailing_filter', 'deadline')
    date_hierarchy = 'date_mailing'


@admin.register(Message)
class MassageAdmin(admin.ModelAdmin):
    list_display = ('pk', 'date', 'status', 'mailing', 'user')
    list_filter = ('status',)
    date_hierarchy = 'date'
