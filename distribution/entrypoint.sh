#!/bin/sh
sleep 15
python manage.py makemigrations --noinput
python manage.py migrate --noinput
python manage.py collectstatic --noinput
python manage.py create_admin
gunicorn distribution.wsgi:application --bind 0.0.0.0:8080